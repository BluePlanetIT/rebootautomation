# Install-Module Posh-SSH -Verbose -Force
# [System.Reflection.Assembly]::Load("System.EnterpriseServices, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")
# $publish = New-Object System.EnterpriseServices.Internal.Publish
# $publish.GacInstall("C:\Program Files\WindowsPowerShell\Modules\Posh-SSH\2.2\Assembly\Renci.SshNet.dll");

if(Test-Path ".\Servers.csv"){}else{
    "SEP=,`nServer,Username,Password,Port`n" | Out-File -FilePath ".\Servers.csv"
}

$DATE = Get-Date -Format "dd/MM/yyyy HH:mm"

$CSV = Import-CSV -Path ".\Servers.csv"

$COUNT = ($CSV | Measure-Object -Property Server).Count

if($COUNT -gt 0){

    ForEach ($SERVER in $CSV){

        $Output = "";

        $Output += "Date: $DATE `n"
        $Output += "Server: $($SERVER.Server) `n"

        $Password = ConvertTo-SecureString $SERVER.Password -AsPlainText -Force
        $Cred = New-Object System.Management.Automation.PSCredential($SERVER.Username, $Password)

        $Session = New-SSHSession -ComputerName $SERVER.Server -Credential $Cred -Port $SERVER.Port -ConnectionTimeout 5 -AcceptKey $true -ErrorAction SilentlyContinue

        if($Session){
            $Output += "Status: Connection Success`n"

            $Command = Invoke-SSHCommand -SshSession $Session -Command "uname -a;reboot" -TimeOut 5

            $Output += "Output: "

            if($Command){

                $Output += $Command.Output

            }else{

                $Output += "N/A (Command Failed)"

            }

            $Output += "`n"

        }else{
            $Output += "Status: Connection Failure`n"

        }

        $Output += "-------------------------------------------------"

        Write-Host $Output

        $Output | Out-File -FilePath ".\Log.txt" -Append
        

    }

}else{

    Write-Host -f Red "There are no hosts specified in 'Servers.csv'."

}