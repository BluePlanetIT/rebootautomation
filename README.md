# Reboot Automation
Restart a list of hosts via SSH. The script can be easily altered to perform any other task en-masse.

### Setup Instructions
 - In an administrative PowerShell window, run `Install-Module Posh-SSH`
 - Run the "Reboot-AllServers.ps1" script to generate required files
 - Edit `Servers.csv` and complete the list of servers
 - Run the script, keep an eye out for `Log.txt` in the script directory

### Task Scheduler
 - Open Windows Task Scheduler
 - Create a new basic task
 - Program: `powershell.exe`
 - Argument: `-File C:\path\to\Reboot-AllServers.ps1`
 - Run whether the user is logged on or not

